#include "GLFW/glfw3.h"
#include "glad/glad.h"
#include <iostream>

void getInput(GLFWwindow* window);

int main() {
	// init glfw
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}

	GLFWwindow * myWindow = glfwCreateWindow(600, 600, "TestWindow", nullptr, nullptr);
	glfwMakeContextCurrent(myWindow);

	// init glad
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	// Log our renderer and OpenGL version
	std::cout << glGetString(GL_RENDERER) << std::endl;
	std::cout << glGetString(GL_VERSION) << std::endl;

	// vao/vbo
	float vertices[] = {
	// Pos				 // colors
	-0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f, // bottom left
	 0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f, // bottom right
	 0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f  // top
	};

	unsigned int VBO, VAO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// positions
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// colors
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3* sizeof(float)));
	glEnableVertexAttribArray(1);


	// shaders
	// vertex
	const char* vertexShaderSource = "#version 420 core\n"
		"layout (location = 0) in vec3 aPos;\n"
		"layout (location = 1) in vec3 aColor;\n"
		"out vec3 ourColor;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = vec4(aPos, 1.0);\n"
		"	ourColor = aColor;\n"
		"}\0";

	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);

	int  success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// fragment
	const char* fragmentShaderSource = "#version 420 core\n"
		"out vec4 FragColor;\n"
		"in vec3 ourColor;\n"
		"void main()\n"
		"{\n"
		"   FragColor = vec4(ourColor, 1.0f);\n"
		"}\n\0";

	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// program
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINK_FAILED\n" << infoLog << std::endl;
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// color timer
	unsigned int timer = 0;

	while (!glfwWindowShouldClose(myWindow)) {
		// poll window
		getInput(myWindow);

		// clear color
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// user shader
		glUseProgram(shaderProgram);

		// color switching
		if (timer == 50) {
			vertices[3] = 1.0f;
			vertices[4] = 0.0f;
			vertices[5] = 0.0f;

			vertices[9] = 0.0f;
			vertices[10] = 1.0f;
			vertices[11] = 0.0f;

			vertices[15] = 0.0f;
			vertices[16] = 0.0f;
			vertices[17] = 1.0f;
		}
		if (timer == 100) {
			vertices[3] = 0.0f;
			vertices[4] = 1.0f;
			vertices[5] = 0.0f;

			vertices[9] = 0.0f;
			vertices[10] = 0.0f;
			vertices[11] = 1.0f;

			vertices[15] = 1.0f;
			vertices[16] = 0.0f;
			vertices[17] = 0.0f;
		}
		if (timer == 150) {
			vertices[3] = 0.0f;
			vertices[4] = 0.0f;
			vertices[5] = 1.0f;

			vertices[9] = 1.0f;
			vertices[10] = 0.0f;
			vertices[11] = 0.0f;

			vertices[15] = 0.0f;
			vertices[16] = 1.0f;
			vertices[17] = 0.0f;
			timer = 0;
		}
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		// bind vao
		glBindVertexArray(VAO);

		// draw
		glDrawArrays(GL_TRIANGLES, 0, 3);

		// swap buffers
		glfwSwapBuffers(myWindow);
		glfwPollEvents();

		timer++;
	}
		
	// delete everything
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glfwTerminate();
	return 0;
}

void getInput(GLFWwindow* window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
}